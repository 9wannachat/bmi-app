
import { Component } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { NativeStorage } from '@ionic-native/native-storage/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {


  public height: any;
  public weight: any;
  public bmi: any = 0;
  public status: any;
  public btnSave = true;
  public uid: any;

  constructor(
    private db: AngularFireDatabase,
    private nativeStorage: NativeStorage
  ) {
    this.db.object('/height').valueChanges().subscribe(res =>{    
      this.height = res;
    });

    this.db.object('/weight').valueChanges().subscribe(res=>{
      this.weight = res;
    });

    this.nativeStorage.getItem('infomation')
    .then(
      res => {
        this.uid = res.uid;
       },
      error => console.error(error)
    );
  }

  calculate(){
    this.btnSave = false;
    const mh = this.height/100;
    this.bmi = this.weight/(mh * mh);

    if(this.bmi < 18.50){
      this.status = 'น้ำหนักน้อย / ผอม';
    }else if(this.bmi > 18.50  && this.bmi < 23){
      this.status = 'ปกติ (สุขภาพดี)';
    }else if(this.bmi >= 23   && this.bmi < 25){
      this.status = 'ท้วม / โรคอ้วนระดับ 1';
    }else if(this.bmi >= 25  && this.bmi < 30){
      this.status = 'อ้วน / โรคอ้วนระดับ 2';
    }else if(this.bmi > 30){
      this.status = 'อ้วนมาก / โรคอ้วนระดับ 3';
    }
  }

  saveHistory(){

    	
    var today = new Date();  
    var date = today.getDate()+'-'+(today.getMonth()+1)+'-'+today.getFullYear();

    const body= {
      height: this.height,
      weight: this.weight,
      bmi: this.bmi,
      uid: this.uid,
      createOn: date
    }

    this.db.list("/history").push(body);
  }

}
