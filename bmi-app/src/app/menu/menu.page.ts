import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  
  page = [
    {
      title: 'Home',
      url: '/menu/home'
    },
    {
      title: 'History',
      url: '/menu/history'
    },
    {
      title: 'Graph',
      url: '/menu/graph'
    },
    {
      title: 'Log Out',
      url: '/menu/logout'
    }
 
  ];

  selectedPath = '';

  constructor(private route: Router) {
    this.route.events.subscribe((event: RouterEvent) => {
      this.selectedPath = event.url;
    });
   }

  ngOnInit() {
  }

}
