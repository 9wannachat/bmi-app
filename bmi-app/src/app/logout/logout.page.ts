import { Component, OnInit } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.page.html',
  styleUrls: ['./logout.page.scss'],
})
export class LogoutPage implements OnInit {

  constructor(
    private route: Router,
    private nativeStorage: NativeStorage
  ) { }

  ngOnInit() {
    this.nativeStorage.setItem('infomation', { key: null, uid: null })
    .then(
      () => {
        this.route.navigate(['/menu/login']);
      }
      
    );
  }

}
