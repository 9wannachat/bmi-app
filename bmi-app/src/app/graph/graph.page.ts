import { Component, OnInit } from '@angular/core';
import { ChartDataSets } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.page.html',
  styleUrls: ['./graph.page.scss'],
})
export class GraphPage implements OnInit {


  chartData: ChartDataSets[] = [{ data: [], label: 'BMI' }];
  chartLabels: Label[];

  chartDataH: ChartDataSets[] = [{ data: [], label: 'Height' }];
 

  chartDataW: ChartDataSets[] = [{ data: [], label: 'Weight' }];
 
 
  // Options
  chartOptions = {
    responsive: true,
    title: {
      display: true,
      text: 'BMI History'
    },
    pan: {
      enabled: false,
      mode: 'xy'
    },
    zoom: {
      enabled: true,
      mode: 'xy'
    }
  };

  chartOptionsH = {
    responsive: true,
    title: {
      display: true,
      text: 'Height History'
    },
    pan: {
      enabled: false,
      mode: 'xy'
    },
    zoom: {
      enabled: true,
      mode: 'xy'
    }
  };

  chartOptionsW = {
    responsive: true,
    title: {
      display: true,
      text: 'Weight History'
    },
    pan: {
      enabled: false,
      mode: 'xy'
    },
    zoom: {
      enabled: true,
      mode: 'xy'
    }
  };

  chartColors: Color[] = [
    {
      borderColor: '#fc9403'
    }
  ];
  chartType = 'line';
  showLegend = false;

  public uid: any;

  constructor(
    private db: AngularFireDatabase,
    private nativeStorage: NativeStorage
  ) {
    this.nativeStorage.getItem('infomation')
    .then(
      res => {
        this.uid = res.uid;
        this.db.list('/history', ref => ref.orderByChild('uid').equalTo(this.uid) && ref.limitToLast(10)).snapshotChanges().subscribe(res => {
          this.chartLabels = [];
          this.chartData[0].data = [];

        
          this.chartDataH[0].data = [];

         
          this.chartDataW[0].data = [];

          res.forEach(item => {
            var i =+1;
            let a = item.payload.toJSON();
            this.chartLabels.push(a['createOn']);
            this.chartData[0].data.push(a['bmi']);

           
            this.chartDataH[0].data.push(a['height']);

           
            this.chartDataW[0].data.push(a['weight']);
            // this.data.push({
            //   height: a['height'],
            //   weight:a['weight'],
            //   bmi: a['bmi']
            // });
          })
        });
      },
      error => console.error(error)
    );
   }

  ngOnInit() {
  }

}
