import { Component, OnInit } from '@angular/core';
import { v4 as uuidv4 } from 'uuid';
import { AlertController } from '@ionic/angular';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-sigup',
  templateUrl: './sigup.page.html',
  styleUrls: ['./sigup.page.scss'],
})
export class SigupPage implements OnInit {

  public fname: any = null;
  public lname: any = null;
  public email: any = null;
  public username: any = null;
  public repass: any = null;
  public password: any = null;

  constructor(
    public alertController: AlertController,
    private db: AngularFireDatabase
  ) { 
   
  }

  ngOnInit() {
  }

  signup(){
    if(this.fname != null && this.lname != null && this.email != null && this.username !== null && this.repass !== null && this.password !== null){

      const body = {
        fname: this.fname,
        lname: this.lname,
        email: this.email,
        username: this.username,
        password: this.password,
        uid: uuidv4()
      }

      this.db.list("/login").push(body);
      this.presentAlert('Signup Success'); 

      this.fname = null;
      this.lname = null;
      this.email = null;
      this.username = null;
      this.repass = null;
      this.password = null;
      
    }else{
      this.presentAlert('Please enter data');     
      return;
    }
  }

  async presentAlert(status) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: status,
      buttons: ['OK']
    });

    await alert.present();
  }

}
