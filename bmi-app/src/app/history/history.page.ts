import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { AngularFireDatabase } from '@angular/fire/database';
import { Component, OnInit } from '@angular/core';

export interface model {
  height: Number,
  weight: Number,
  bmi: Number
}

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {

  public uid: any;
  public data:model[] = [];

  constructor(
    private db: AngularFireDatabase,
    private nativeStorage: NativeStorage
  ) {
    this.nativeStorage.getItem('infomation')
      .then(
        res => {
          this.uid = res.uid;
          this.db.list('/history', ref => ref.orderByChild('uid').equalTo(this.uid)).snapshotChanges().subscribe(res => {
           
            res.forEach(item => {
              let a = item.payload.toJSON();
              this.data.push({
                height: a['height'],
                weight:a['weight'],
                bmi: a['bmi']
              });
            })
          });
        },
        error => console.error(error)
      );
  }

  ngOnInit() {
    // this.db.list('/login/', ref => ref.orderByChild('uid').equalTo(this.uid) ).snapshotChanges().subscribe(items => {
    //   // items.forEach(item => {
    //   //   let data = item.payload.toJSON();

    //   // })
    // });
  }

}
