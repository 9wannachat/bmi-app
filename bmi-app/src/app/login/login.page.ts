import { AlertController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireDatabase } from '@angular/fire/database';
import { NativeStorage } from '@ionic-native/native-storage/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public data: any;
  public username: any = null;
  public password: any = null;

  constructor(
    private route: Router,
    public alertController: AlertController,
    private db: AngularFireDatabase,
    private nativeStorage: NativeStorage
  ) { }

  ngOnInit() {
     this.nativeStorage.getItem('infomation')
    .then(
      res => {
        if(res.uid !== null){
          this.route.navigate(['/menu/home']);
        }else{
         return
        }
       },
      error => console.error(error)
    );
  }

  nextpage() {
    this.route.navigate(['/menu/signup']);
  }

  signin() {
    if (this.username !== null && this.password !== null) {
      this.db.list('/login/', ref => ref.orderByChild('username').equalTo(this.username) && ref.orderByChild('password').equalTo(this.password)).snapshotChanges().subscribe(items => {
        items.forEach(item => {
          let data = item.payload.toJSON();
          console.log(data['uid']);
          if (items.length !== 0) {
            this.nativeStorage.setItem('infomation', { key: data['$key'], uid: data['uid'] })
              .then(
                () => {
                  this.route.navigate(['/menu/home']);
                },
                error => this.presentAlert("Not Store data")
              );
          } else {
            this.presentAlert("Username or Password invalid")
          }
        })

        if (items.length === 0) {
          this.presentAlert("Username or Password invalid")
        }
      });
    }else{
      this.presentAlert("Please Enter data")
    }

    // this.nativeStorage.getItem('infomation')
    // .then(
    //   res => { this.username = res.uid },
    //   error => console.error(error)
    // );
  }

  async presentAlert(status) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: status,
      buttons: ['OK']
    });

    await alert.present();
  }



}
