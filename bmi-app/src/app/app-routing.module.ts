import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

// const routes: Routes = [
//   { path: '', redirectTo: 'home', pathMatch: 'full' },
//   { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
//   {
//     path: 'menu',
//     loadChildren: () => import('./pages/menu/menu.module').then( m => m.MenuPageModule)
//   },
// {
//   path: 'login',
//   loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
// },
// {
//   path: 'history',
//   loadChildren: () => import('./history/history.module').then( m => m.HistoryPageModule)
// },
// {
//   path: 'graph',
//   loadChildren: () => import('./graph/graph.module').then( m => m.GraphPageModule)
// },
// {
//   path: 'sigup',
//   loadChildren: () => import('./sigup/sigup.module').then( m => m.SigupPageModule)
// },
// ];

const routes: Routes = [
  { path: '', loadChildren: './menu/menu.module#MenuPageModule' },
  {
    path: 'logout',
    loadChildren: () => import('./logout/logout.module').then( m => m.LogoutPageModule)
  },
 
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
