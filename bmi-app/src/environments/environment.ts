// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyDurkCv72Pb5qHm5GAKDSP2wve-xHetfXY",
    authDomain: "bmi-app-6c629.firebaseapp.com",
    databaseURL: "https://bmi-app-6c629.firebaseio.com",
    projectId: "bmi-app-6c629",
    storageBucket: "bmi-app-6c629.appspot.com",
    messagingSenderId: "226585690712",
    appId: "1:226585690712:web:3ae70abd30f7237026c02d",
    measurementId: "G-CE5PFY49LD"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
